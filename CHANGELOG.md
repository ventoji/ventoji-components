# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://github.com/ventoji/ventoji-components/compare/v1.1.1...v1.2.0) (2021-01-26)


### Features

* adding automation tasks for ci and cd ([59c84f5](https://github.com/ventoji/ventoji-components/commit/59c84f5a178e8a196601a6dd4a94c4bdb1df2e98))


### Bug Fixes

* fixing jest config file properly ([570a710](https://github.com/ventoji/ventoji-components/commit/570a7105bb8885b977298f8a4851bdbcd5aaa932))

### [1.1.1](https://github.com/ventoji/ventoji-components/compare/v1.1.0...v1.1.1) (2021-01-26)
